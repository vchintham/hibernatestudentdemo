package com.hibernatelearning.hibernatestudentdemo.controller;

import com.hibernatelearning.hibernatestudentdemo.model.dao.Student;
import com.hibernatelearning.hibernatestudentdemo.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping(value = "/addStudent")
    private String addStudent(@RequestBody Student student) {
        log.info("Adding Student");
        return studentService.addStudent(student);
    }

    @GetMapping(value = "/students/{studentId}")
    private Student getStudent(@PathVariable String studentId) {
        log.info("Getting student with id: {}", studentId);
        return studentService.getStudent(studentId);
    }
}
