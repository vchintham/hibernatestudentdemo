package com.hibernatelearning.hibernatestudentdemo.controller;

import com.hibernatelearning.hibernatestudentdemo.model.dao.Passport;
import com.hibernatelearning.hibernatestudentdemo.service.PassportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PassportController {

    private final PassportService passportService;

    @PostMapping(value = "/addPassport")
    private String addPassport(@RequestBody Passport passport) {
        log.info("Adding Passport");
        return passportService.addPassport(passport);
    }

    @GetMapping(value = "/passports/{passportId}")
    private Passport getPassport(@PathVariable String passportId) {
        log.info("Getting passport with id: {}", passportId);
        return passportService.getPassport(passportId);
    }
}
