package com.hibernatelearning.hibernatestudentdemo.model.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Passport {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long id;
    private String passportNumber;
    private String country;
    @OneToOne (fetch = FetchType.LAZY, mappedBy = "passport")
    private Student student;
}
