package com.hibernatelearning.hibernatestudentdemo.service;

import com.hibernatelearning.hibernatestudentdemo.model.dao.Passport;
import com.hibernatelearning.hibernatestudentdemo.model.dao.Student;
import com.hibernatelearning.hibernatestudentdemo.repository.PassportRepository;
import com.hibernatelearning.hibernatestudentdemo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final PassportRepository passportRepository;

    public String addStudent(Student student) {
        if(student.getPassport().getId() > 0) {
            log.info("Student Passport Id passed in the request: {}", student.getPassport().getId());
            final Optional<Passport> passport = passportRepository.findById(student.getPassport().getId());
            if(passport.isPresent()) {
                Passport passportRetrieved = passport.get();
                Passport passport1 = new Passport();
                passport1.setId(passportRetrieved.getId());
                student.setPassport(passport1);
            }
        }
        final Student studentAdded = studentRepository.save(student);
        log.info("Student added with id: {}", studentAdded.getId());
        return "Student Added with id: " + studentAdded.getId();
    }

    public Student getStudent(String studentId) {
        final Optional<Student> student = studentRepository.findById(Long.parseLong(studentId));
        if(student.isPresent()) {
            final Student studentRetrieved = student.get();
            return studentRetrieved;
        }
        return null;
    }
}
