package com.hibernatelearning.hibernatestudentdemo.service;

import com.hibernatelearning.hibernatestudentdemo.model.dao.Passport;
import com.hibernatelearning.hibernatestudentdemo.repository.PassportRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class PassportService {

    private final PassportRepository passportRepository;

    public String addPassport(Passport passport) {
        final Passport passportAdded = passportRepository.save(passport);
        log.info("Passport added with id: {}", passportAdded.getId());
        return "Passport Added with id: " + passportAdded.getId();
    }

    public Passport getPassport(String passportId) {
        final Optional<Passport> passport = passportRepository.findById(Long.parseLong(passportId));
        if(passport.isPresent()) {
            final Passport passportRetrieved = passport.get();
            log.info("Passport Retrieved: {}", passportRetrieved.toString());
            passportRetrieved.getStudent();
            return passportRetrieved;
        }
        return null;
    }
}
