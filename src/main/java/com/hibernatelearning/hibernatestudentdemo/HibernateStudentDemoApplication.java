package com.hibernatelearning.hibernatestudentdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
@EnableJpaAuditing
@Slf4j
public class HibernateStudentDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernateStudentDemoApplication.class, args);
	}

}
