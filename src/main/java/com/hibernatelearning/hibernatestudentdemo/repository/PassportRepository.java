package com.hibernatelearning.hibernatestudentdemo.repository;

import com.hibernatelearning.hibernatestudentdemo.model.dao.Passport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassportRepository extends JpaRepository<Passport, Long> {
}
