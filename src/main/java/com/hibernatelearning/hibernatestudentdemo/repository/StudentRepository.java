package com.hibernatelearning.hibernatestudentdemo.repository;

import com.hibernatelearning.hibernatestudentdemo.model.dao.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
}
